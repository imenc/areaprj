from rest_framework import serializers
from apps.area.models import Provinsi, Kabupaten, Kecamatan, Desa


class ProvinsiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provinsi
        fields = ['id', 'nama']


class KabupatenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kabupaten
        fields = ['id', 'id_prov', 'nama']


class KecamatanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kecamatan
        fields = ['id', 'id_kabupaten', 'nama']


class DesaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Desa
        fields = ['id', 'id_kecamatan', 'nama']


