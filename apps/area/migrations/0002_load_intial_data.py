# Generated by Django 2.2.4 on 2019-09-04 08:47
from django.core.management import call_command
from django.db import migrations, models


def load_provinsi_from_fixture(apps, schema_editor):
    call_command("loaddata", "provinsi.json")


def load_kabupaten_from_fixture(apps, schema_editor):
    call_command("loaddata", "kabupaten.json")


def load_kecamatan_from_fixture(apps, schema_editor):
    call_command("loaddata", "kecamatan.json")


def load_desa_from_fixture(apps, schema_editor):
    call_command("loaddata", "desa.json")


def reverse_data(apps, schema_editor):
    pass


class Migration(migrations.Migration):

    dependencies = [
        ('area', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_provinsi_from_fixture, reverse_code=reverse_data),
        migrations.RunPython(load_kabupaten_from_fixture, reverse_code=reverse_data),
        migrations.RunPython(load_kecamatan_from_fixture, reverse_code=reverse_data),
        migrations.RunPython(load_desa_from_fixture, reverse_code=reverse_data)
    ]
