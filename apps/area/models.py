from django.db import models


class Provinsi(models.Model):
    id = models.CharField(primary_key=True, max_length=2)
    nama = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'Provinsi'
        verbose_name = 'Provinsi'
        verbose_name_plural = 'Provinsi'
        app_label = 'area'
        ordering = ['id']

    def __str__(self):
        return '%s - %s' % (self.id, self.nama)


class Kabupaten(models.Model):
    id = models.CharField(primary_key=True, max_length=4)
    id_prov = models.ForeignKey(Provinsi, db_column='id_prov', on_delete=models.CASCADE,
                                related_name='%(app_label)s_%(class)s_related')
    nama = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'Kabupaten'
        verbose_name = 'Kabupaten'
        verbose_name_plural = 'Kabupatens'
        app_label = 'area'
        ordering = ['id']

    def __str__(self):
        return '%s - %s' % (self.id, self.nama)


class Kecamatan(models.Model):
    id = models.CharField(primary_key=True, max_length=7)
    id_kabupaten = models.ForeignKey(Kabupaten, db_column='id_kabupaten', on_delete=models.CASCADE,
                                     related_name='%(app_label)s_%(class)s_related')
    nama = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'Kecamatan'
        verbose_name = 'Kecamatan'
        verbose_name_plural = 'Kecamatans'
        app_label = 'area'
        ordering = ['id']

    def __str__(self):
        return '%s - %s' % (self.id, self.nama)


class Desa(models.Model):
    id = models.CharField(primary_key=True, max_length=10)
    id_kecamatan = models.ForeignKey(Kecamatan, db_column='id_kecamatan', on_delete=models.CASCADE,
                                     related_name='%(app_label)s_%(class)s_related')
    nama = models.CharField(max_length=100)

    class Meta:
        managed = True
        db_table = 'Desa'
        verbose_name = 'Desa'
        verbose_name_plural = 'Desas'
        app_label = 'area'
        ordering = ['id']

    def __str__(self):
        return '%s - %s' % (self.id, self.nama)

