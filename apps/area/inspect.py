# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Desa(models.Model):
    id = models.CharField(primary_key=True, max_length=10)
    id_kecamatan = models.CharField(max_length=7, blank=True, null=True)
    nama = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Desa'


class Kabupaten(models.Model):
    id = models.CharField(primary_key=True, max_length=4)
    id_prov = models.CharField(max_length=2)
    nama = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'Kabupaten'


class Kecamatan(models.Model):
    id = models.CharField(primary_key=True, max_length=7)
    id_kabupaten = models.CharField(max_length=4)
    nama = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'Kecamatan'


class Provinsi(models.Model):
    id = models.CharField(primary_key=True, max_length=2)
    nama = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'Provinsi'
