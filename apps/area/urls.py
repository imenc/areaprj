from django.conf.urls import url
from apps.area import views


urlpatterns = [
    url(r'^provinsi/$', views.ProvinsiListView.as_view()),
    url(r'^provinsi/(?P<pk>\w+)/$', views.ProvinsiDetailView.as_view()),
    url(r'^kabupaten/$', views.KabupatenListView.as_view()),
    url(r'^kabupaten/(?P<pk>\w+)/$', views.KabupatenDetailView.as_view()),
    url(r'^kecamatan/$', views.KecamatanListView.as_view()),
    url(r'^kecamatan/(?P<pk>\w+)/$', views.KecamatanDetailView.as_view()),
    url(r'^desa/$', views.DesaListView.as_view()),
    url(r'^desa/(?P<pk>\w+)/$', views.DesaDetailView.as_view()),
]

