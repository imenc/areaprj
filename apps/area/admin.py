from django.contrib import admin
from .models import Provinsi, Kabupaten, Kecamatan, Desa


class ProvinsiAdmin(admin.ModelAdmin):
    list_display = ('id', 'nama')
    fields = ('id', 'nama')


class KabupatenAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_prov', 'nama')
    fields = ('id', 'id_prov', 'nama')


class KecamatanAdmin(admin.ModelAdmin):
    list_display = ('id','id_kabupaten','nama')
    fields = ('id', 'id_kabupaten', 'nama')


class DesaAdmin(admin.ModelAdmin):
    list_display = ('id', 'id_kecamatan', 'nama')
    fields = ('id', 'id_kecamatan', 'nama')


admin.site.register(Provinsi, ProvinsiAdmin)
admin.site.register(Kabupaten, KabupatenAdmin)
admin.site.register(Kecamatan, KecamatanAdmin)
admin.site.register(Desa, DesaAdmin)
