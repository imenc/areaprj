from rest_framework import generics
from apps.area.models import Provinsi, Kabupaten, Kecamatan, Desa
from apps.area.serializers import ProvinsiSerializer, KabupatenSerializer, KecamatanSerializer, DesaSerializer

# Added pagination
from areaprj.helper.paginator import StandardResultsSetPagination, LargeResultsSetPagination


class ProvinsiListView(generics.ListCreateAPIView):
    serializer_class = ProvinsiSerializer

    def get_queryset(self):
        return Provinsi.objects.order_by('id')


class ProvinsiDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ProvinsiSerializer

    def get_queryset(self):
        return Provinsi.objects.all()


class KabupatenListView(generics.ListCreateAPIView):
    serializer_class = KabupatenSerializer

    def get_queryset(self):
        return Kabupaten.objects.order_by('id')


class KabupatenDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = KabupatenSerializer

    def get_queryset(self):
        return Kabupaten.objects.all()


class KecamatanListView(generics.ListCreateAPIView):
    serializer_class = KecamatanSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return Kecamatan.objects.order_by('id')


class KecamatanDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = KecamatanSerializer

    def get_queryset(self):
        return Kecamatan.objects.all()


class DesaListView(generics.ListCreateAPIView):
    serializer_class = DesaSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self):
        return Desa.objects.order_by('id')


class DesaDetailView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = DesaSerializer

    def get_queryset(self):
        return Desa.objects.all()